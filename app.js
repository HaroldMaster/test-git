const http = require("http");
const host = "127.0.0.1";
const port = 3000;
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-type", "Text/plain");
  res.end("Hola mundo - Mensaje de Bienvenida");
});
server.listen(port, host, ()=>{
    console.log(`Esta escuchando sobre http://${host}:${port}`);
});